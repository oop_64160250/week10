public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;
    public Triangle(double a, double b, double c) {
        super("Rectangle");
        this.a = a;
        this.b = b;
        this.c = c;

    }
    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }
    @Override
    public String toString() {
        return this.getName() + "a:"+ this.a + "b:" + this.b + "c:" + this.c;
    }
    @Override
    public double calArea() {
        return this.a * this.b * this.c * 1/2;
    }
    

    @Override
    public double calPerimeter() {
        return this.a + this.b + this.c;
    }
}
